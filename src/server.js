const express = require("express");
const dotenv = require('dotenv');
const cors = require("cors");
const HttpException = require('./utils/HttpException.utils');
const errorMiddleware = require('./middleware/error.middleware');
const userRouter = require('./routes/user.route');
const signalRouter = require('./routes/signal.route');
const subjectRouter = require('./routes/subject.route');
const eventRouter = require('./routes/event.route');

// Init express
const app = express();
// Init environment
dotenv.config();
// parse requests of content-type: application/json
// parses incoming requests with JSON payloads
app.use(express.json());
// enabling cors for all requests by using cors middleware
app.use(cors());
app.use(express.urlencoded({ extended: true }));
// Enable pre-flight
app.options("*", cors());

global.__basedir = __dirname + "/..";

const port = Number(process.env.PORT || 3331);

app.use(`/api/v1/users`, userRouter);
app.use(`/api/v1/signals`, signalRouter);
app.use(`/api/v1/subjects`, subjectRouter);
app.use(`/api/v1/events`, eventRouter);


// 404 error
app.all('*', (req, res, next) => {
    const err = new HttpException(404, 'Endpoint Not Found');
    next(err);
});

// Error middleware
app.use(errorMiddleware);

// starting the server
app.listen(port, () =>
    console.log(`🚀 Server running on port ${port}!`));


module.exports = app;