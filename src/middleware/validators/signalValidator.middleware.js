const { body } = require('express-validator');

exports.createSignalSchema = [
    body('eventId')
        .exists()
        .withMessage('eventId is require')
        .isNumeric()
        .withMessage('Must be a number'),
    body('subjectId')
        .exists()
        .withMessage('key is require')
        .isNumeric()
        .withMessage('Must be a number'),
    body('uuid')
        .exists()
        .withMessage('Uuid is required'),
    body('numberOfParticipant')
        .optional()
        .isNumeric()
        .withMessage('Must be a number'),
    body('createBy')
        .exists()
        .withMessage('User require')
        .isNumeric()
        .withMessage('Must be a number'),
    body('description')
        .optional()
        .isLength({ max: 255})
        .withMessage('Description can contain max 255 characters'),
];

exports.updateSignalSchema = [
    body('eventId')
        .exists()
        .withMessage('eventId is require')
        .isNumeric()
        .withMessage('Must be a number'),
    body('subjectId')
        .exists()
        .withMessage('key is require')
        .isNumeric()
        .withMessage('Must be a number'),
    body('uuid')
        .exists()
        .withMessage('Uuid is required'),
    body('numberOfParticipant')
        .optional()
        .isNumeric()
        .withMessage('Must be a number'),
    body('createBy')
        .exists()
        .withMessage('User require')
        .isNumeric()
        .withMessage('Must be a number'),
    body('description')
        .optional()
        .isLength({ max: 255 })
        .withMessage('Description can contain max 255 characters'),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
        .custom(value => {
            const updates = Object.keys(value);
            const allowUpdates = ['eventId', 'subjectId', 'uuid', 'createBy', 'description'];
            return updates.every(update => allowUpdates.includes(update));
        })
        .withMessage('Invalid updates!')
];
