const { body } = require('express-validator');

exports.createEventSchema = [
    body('userId')
        .exists()
        .withMessage('user id is require')
        .isNumeric()
        .withMessage('Must be number'),
    body('eventId')
        .exists()
        .withMessage('event id is require')
        .isNumeric()
        .withMessage('Must be number'),
    body('subjectId')
        .exists()
        .withMessage('subject id is require')
        .isNumeric()
        .withMessage('Must be number'),
];

exports.updateEventSchema = [
    body('userId')
        .exists()
        .withMessage('user id is require')
        .isNumeric()
        .withMessage('Must be number'),
    body('eventId')
        .exists()
        .withMessage('event id is require')
        .isNumeric()
        .withMessage('Must be number'),
    body('subjectId')
        .exists()
        .withMessage('subject id is require')
        .isNumeric()
        .withMessage('Must be number'),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
        .custom(value => {
            const updates = Object.keys(value);
            const allowUpdates = ['userId', 'subjectId', 'eventId'];
            return updates.every(update => allowUpdates.includes(update));
        })
        .withMessage('Invalid updates!')
];
