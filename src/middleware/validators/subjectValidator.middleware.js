const { body } = require('express-validator');

exports.createSubjectSchema = [
    body('name')
        .exists()
        .withMessage('Subject name is require'),
    body('semester')
        .exists()
        .withMessage('semester is require'),
    body('teacherId')
        .exists()
        .withMessage('teacherId is required'),
    body('numberOfParticipant')
        .optional()
        .isNumeric()
        .withMessage('Must be a number'),
    body('startTime')
        .exists()
        .withMessage('Start time is require'),
    body('startTime')
        .exists()
        .withMessage('Start time is require'),
    body('description')
        .optional()
        .isLength({ max: 255 })
        .withMessage('Description can contain max 255 characters'),
];

exports.updateSubjectSchema = [
    body('name')
        .exists()
        .withMessage('Subject name is require'),
    body('semester')
        .exists()
        .withMessage('semester is require'),
    body('teacherId')
        .exists()
        .withMessage('teacherId is required'),
    body('numberOfParticipant')
        .optional()
        .isNumeric()
        .withMessage('Must be a number'),
    body('startTime')
        .exists()
        .withMessage('Start time is require'),
    body('endTime')
        .exists()
        .withMessage('End time is require'),
    body('description')
        .optional()
        .isLength({ max: 255 })
        .withMessage('Description can contain max 255 characters'),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
        .custom(value => {
            const updates = Object.keys(value);
            const allowUpdates = ['name', 'teacherId', 'numberOfParticipant', 'startTime', 'endTime', 'description'];
            return updates.every(update => allowUpdates.includes(update));
        })
        .withMessage('Invalid updates!')
];
