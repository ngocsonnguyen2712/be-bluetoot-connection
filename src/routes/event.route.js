const express = require('express');
const router = express.Router();
const eventController = require('../controllers/event.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createEventSchema, updateEventSchema } = require('../middleware/validators/event.middleware');


router.get('/', auth(), awaitHandlerFactory(eventController.getAllEvent));
router.get('/:id', auth(), awaitHandlerFactory(eventController.getEventById));
router.get('/subject/:subjectId', auth(Role.admin, Role.teacher), awaitHandlerFactory(eventController.getAllEventBySubjectId));
router.post('/', auth(), createEventSchema, awaitHandlerFactory(eventController.createEvent));
router.patch('/:id', auth(), updateEventSchema, awaitHandlerFactory(eventController.updateEvent));
router.delete('/:id', auth(Role.admin, Role.teacher), awaitHandlerFactory(eventController.deleteEvent));

module.exports = router;