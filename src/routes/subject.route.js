const express = require('express');
const router = express.Router();
const subjectController = require('../controllers/subject.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createSubjectSchema, updateSubjectSchema } = require('../middleware/validators/subjectValidator.middleware');


router.get('/', auth(Role.admin), awaitHandlerFactory(subjectController.getAllSubjects));
router.get('/:id', auth(Role.admin, Role.teacher), awaitHandlerFactory(subjectController.getSubjectById));
router.post('/', auth(Role.admin), createSubjectSchema, awaitHandlerFactory(subjectController.createSubject));
router.patch('/:id', auth(Role.admin), updateSubjectSchema, awaitHandlerFactory(subjectController.updateSubject));
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(subjectController.deleteSubject));

module.exports = router;