const express = require('express');
const router = express.Router();
const signalController = require('../controllers/signal.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createSignalSchema, updateSignalSchema } = require('../middleware/validators/signalValidator.middleware');


router.get('/', auth(), awaitHandlerFactory(signalController.getAllSignals));
router.get('/:id', auth(), awaitHandlerFactory(signalController.getSignalById));
router.post('/', auth(Role.admin, Role.teacher), createSignalSchema, awaitHandlerFactory(signalController.createSignal)); 
router.patch('/:id', auth(Role.admin, Role.teacher), updateSignalSchema, awaitHandlerFactory(signalController.updateSignal));
router.delete('/:id', auth(Role.admin, Role.teacher), awaitHandlerFactory(signalController.deleteSignal));

module.exports = router;