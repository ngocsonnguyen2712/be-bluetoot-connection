DROP DATABASE IF EXISTS bluetooth_connection;   
CREATE DATABASE IF NOT EXISTS bluetooth_connection;   
USE bluetooth_connection; 

DROP TABLE IF EXISTS user;

CREATE TABLE IF NOT EXISTS user 
  ( 
     id         INT PRIMARY KEY auto_increment, 
     username   VARCHAR(25) UNIQUE NOT NULL, 
     password   CHAR(60) NOT NULL, 
     firstName VARCHAR(50) NOT NULL, 
     lastName  VARCHAR(50) NOT NULL, 
     email      VARCHAR(100) UNIQUE NOT NULL, 
     role       ENUM('admin', 'teacher', 'student') DEFAULT 'student', 
     age        INT(11) DEFAULT 0 
  ); 

DROP TABLE IF EXISTS signals;

CREATE TABLE IF NOT EXISTS signals
(
   id						          INT PRIMARY KEY auto_increment,
   signalKey				      VARCHAR(10) UNIQUE NOT NULL,
   title					        VARCHAR(25) UNIQUE NOT NULL, 
   uuid					          VARCHAR(36) UNIQUE NOT NULL, 
   numberOfParticipant		INT(4) DEFAULT 0, 
   userId					        INT(11) UNIQUE NOT NULL,
   description          	VARCHAR(255),
   createAt					      TIMESTAMP NOT NULL
);


