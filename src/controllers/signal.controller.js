const SignalModel = require('../models/signal.model');
const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Signal Controller
 ******************************************************************************/
class SignalController {
    getAllSignals = async (req, res, next) => {
        let signalList = await SignalModel.find();
        if (!signalList.length) {
            throw new HttpException(404, 'Signals not found');
        }
        res.send(signalList);
    };

    getSignalById = async (req, res, next) => {
        const signal = await SignalModel.findOne({ id: req.params.id });
        if (!signal) {
            throw new HttpException(404, 'Signal not found');
        }

        res.send(signal);
    };

    createSignal = async (req, res, next) => {
        this.checkValidation(req);
        
        const result = await SignalModel.create(req.body);

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        res.status(201).send('Signal was created!');
    };

    updateSignal = async (req, res, next) => {
        this.checkValidation(req);
        const result = await SignalModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows, changedRows, info } = result;

        const message = !affectedRows ? 'Signal not found' :
            affectedRows && changedRows ? 'Signal updated successfully' : 'Updated failed';

        res.send({ message, info });
    };

    deleteSignal = async (req, res, next) => {
        const result = await SignalModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Signal not found');
        }
        res.send('Signal has been deleted');
    };

    checkValidation = (req) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            throw new HttpException(400, 'Validation faild', errors);
        }
    }

}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new SignalController;