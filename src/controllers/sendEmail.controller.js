const nodemailer = require('nodemailer')

let otp = Math.random();
otp = otp * 1000000;
otp = parseInt(otp);

const transporter = nodemailer.createTransport({
	host: "smtp.gmail.com",
	port: 465,
	secure: true,
	service: 'gmail',

	auth: {
		user: process.env.EMAIL_USER,
		pass: process.env.EMAIL_PASSWORD,
	}
});


const sendEmail = (email) => {

	// send mail with defined transport object
	const mailOptions = {
		to: email,
		subject: "Otp for registration is: ",
		html: "<h3>OTP for account verification is </h3>" + "<h1 style='font-weight:bold;'>" + otp + "</h1>" // html body
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			return console.log(error);
		}
		console.log('Message sent: %s', info.messageId);
		console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

		return info;
	});
}

const verifyCode = (otpConfirm) => {
	if (otpConfirm === otp) {
		return true;
	}
	else {
		return false;
	}
}

const resendCode = (email) => {
	const mailOptions = {
		to: email,
		subject: "Otp for registration is: ",
		html: "<h3>OTP for account verification is </h3>" + "<h1 style='font-weight:bold;'>" + otp + "</h1>" // html body
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.log(error);
			return false;
		}
		console.log('Message sent: %s', info.messageId);
		console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
		return true;
	});
}

module.exports = {
	sendEmail,
	verifyCode,
	resendCode
}