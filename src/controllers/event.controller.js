const EventModel = require('../models/event.model');
const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Event Controller
 ******************************************************************************/
class EventController {
    getAllEvent = async (req, res, next) => {
        let eventList = await EventModel.find();
        if (!eventList.length) {
            throw new HttpException(404, 'Events not found');
        }
        res.send(eventList);
    };

    getAllEventBySubjectId = async (req, res, next) => {
        let eventList = await EventModel.find({ subjectId: req.params.subjectId});
        if (!eventList.length) {
            throw new HttpException(404, 'Events not found');
        }
        res.send(eventList);
    }

    getEventById = async (req, res, next) => {
        const event = await EventModel.findOne({ id: req.params.id });
        if (!event) {
            throw new HttpException(404, 'Event not found');
        }
        res.send(event);
    };

    createEvent = async (req, res, next) => {
        this.checkValidation(req);
        const result = await EventModel.create(req.body);
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        res.status(201).send('Event was created!');
    };

    updateEvent = async (req, res, next) => {
        this.checkValidation(req);
        const result = await EventModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows, changedRows, info } = result;

        const message = !affectedRows ? 'Event not found' :
            affectedRows && changedRows ? 'Event updated successfully' : 'Updated failed';

        res.send({ message, info });
    };

    deleteEvent = async (req, res, next) => {
        const result = await EventModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Event not found');
        }
        res.send('Event has been deleted');
    };

    checkValidation = (req) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            throw new HttpException(400, 'Validation fail', errors);
        }
    }

}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new EventController;