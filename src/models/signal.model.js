const query = require('../db/db-connection');
const { multipleColumnSet } = require('../utils/common.utils');
class SignalModel {
    tableName = 'signals';

    find = async (params = {}) => {
        let sql = `SELECT * FROM ${this.tableName}`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        const { columnSet, values } = multipleColumnSet(params)
        sql += ` WHERE ${columnSet}`;

        return await query(sql, [...values]);
    }

    findOne = async (params) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

        const result = await query(sql, [...values]);

        // return back the first row (signal)
        return result[0];
    }

    create = async ({ eventId, subjectId, uuid, numberOfParticipant = 0, createBy, description }) => {
        const sql = `INSERT INTO ${this.tableName}
        (eventId ,subjectId, uuid, numberOfParticipant, createBy, description) VALUES (?,?,?,?,?,?)`;
        const result = await query(sql, [eventId, subjectId, uuid, numberOfParticipant, createBy, description]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }

    update = async (params, id) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `UPDATE signals SET ${columnSet} WHERE id = ?`;

        const result = await query(sql, [...values, id]);

        return result;
    }

    delete = async (id) => {
        const sql = `DELETE FROM ${this.tableName}
        WHERE id = ?`;
        const result = await query(sql, [id]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
}

module.exports = new SignalModel;